# advanced-python-homework-2023
## Установка зависимостей для разработки

Для разработки рекомендуется использовать виртуальное окружение. Для его создания и активации выполните следующие команды:

```bash
# Создание виртуального окружения (в директории venv)
python -m virtualenv venv

# Активация виртуального окружения в Linux/MacOS
source venv/bin/activate

# Активация виртуального окружения в Windows
venv\Scripts\activate

После активации виртуального окружения установите зависимости разработки:
# Для проектов с использованием poetry
poetry install

# Для проектов с использованием pip
pip install -r requirements-dev.txt
Теперь в вашем виртуальном окружении будут установлены Sphinx, Pylint и MyPy, которые рекомендуется использовать для разработки проекта.



## Генерация документации

#Документация создана с использованием Sphinx. Для ее перегенерации выполните следующие шаги:

cd advanced-python-homework-2023
mkdir doc
cd doc
sphinx-quickstart
cd ..
sphinx-apidoc -o doc .
cd doc
rm modules.rst
rm setup.rst

#Отредактируйте файл docs/conf.py и добавьте следующий код:

import os
import sys
sys.path.insert(0, os.path.abspath('../'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',  # Для поддержки Google-style docstrings
]


#Выполните команду для генерации документации:

    cd ..
    make html
#Open file:///home/timur/Gitea/advanced-python-homework-2023/doc/build/html/search.html  THAT`s ALL FOR TODAY)
